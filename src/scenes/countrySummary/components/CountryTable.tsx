import React from "react";
import axios from 'axios';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from '@material-ui/core/TablePagination';
import {Backdrop, CircularProgress, Paper} from "@material-ui/core";

type Props = {
    slug: string
}

type State = {
    dates: {
        Confirmed: number,
        Deaths: number,
        Recovered: number,
        Active: number,
        Date: string
    }[],
    page: number,
    size: number,
    totalPages: number,
    count: number,
    loading: boolean
}

class CountryTable extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = { dates: [], page: 0, size: 0, totalPages: 1, count: 0, loading: false};
    }

    componentDidMount() {
        this.setState({loading: true});
        axios.get(`${process.env.BACKEND_ADDR}:${process.env.BACKEND_PORT}/statistics/${this.props.slug}`)
            .then(res => {
                this.setState({
                    dates: res.data.response,
                    count: res.data.count,
                    size: res.data.size,
                    totalPages: res.data.totalPages,
                    page: res.data.page,
                    loading: false})
            })
    }

    onPageChange = (_: any, page: number) => {
        axios.get(`${process.env.BACKEND_ADDR}:${process.env.BACKEND_PORT}/statistics/${this.props.slug}?page=${page}&size=${this.state.size}`)
            .then(res => {
                this.setState({
                    dates: res.data.response,
                    count: res.data.count,
                    size: res.data.size,
                    totalPages: res.data.totalPages,
                    page: res.data.page})
            })
    }

    onRowsChange = (e: any) => {
        axios.get(`${process.env.BACKEND_ADDR}:${process.env.BACKEND_PORT}/statistics/${this.props.slug}?page=${0}&size=${e.target.value}`)
            .then(res => {
                this.setState({
                    dates: res.data.response,
                    count: res.data.count,
                    size: res.data.size,
                    totalPages: res.data.totalPages,
                    page: res.data.page})
            })
    }

    render() {
        return <TableContainer component={Paper}>
            <Table size={'small'}>
                <TableHead>
                    <TableRow>
                        <TableCell>Fecha</TableCell>
                        <TableCell>Casos Confirmados</TableCell>
                        <TableCell>Total de Muertes</TableCell>
                        <TableCell>Total de Recuperados</TableCell>
                        <TableCell>Casos Activos</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {this.state.dates.map((row, idx) => (
                        <TableRow key={idx}>
                            <TableCell>{row.Date.split("T")[0]}</TableCell>
                            <TableCell align='center'>{row.Confirmed}</TableCell>
                            <TableCell align='center'>{row.Deaths}</TableCell>
                            <TableCell align='center'>{row.Recovered}</TableCell>
                            <TableCell align='center'>{row.Active}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
                <TableFooter>
                    <TablePagination count={this.state.count} page={this.state.page} rowsPerPage={this.state.size}
                                     onChangePage={this.onPageChange} onChangeRowsPerPage={this.onRowsChange}
                                     labelRowsPerPage={"Filas por página"}
                                     labelDisplayedRows={({from, to, count}) => (`${from}-${to} de ${count}`)}
                    />
                </TableFooter>
            </Table>
            <Backdrop open={this.state.loading} style={{zIndex: 999}}>
                <CircularProgress />
            </Backdrop>
        </TableContainer>
    }

}

export default CountryTable;
