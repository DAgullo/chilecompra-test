import React from "react";

import {Grid, Paper} from "@material-ui/core";
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import CountryTable from "./components/CountryTable";

type Props = {
    slug: string
}

const capitalize = (str: string, lower = false) =>
    (lower ? str.toLowerCase() : str).replace(/(?:^|\s|["'([{])+\S/g, match => match.toUpperCase());
;

function slugToName (slug: string) {
    const notCapitalized = ['and', 'of'];
    let splitted = slug.split('-');
    return splitted.map((word: string) => (notCapitalized.includes(word) ? word : capitalize(word)) )
        .join(" ");
}

function CountrySummary (props: Props) {

    return <Grid container justify="flex-start" spacing={2}>
        <Grid item xs={12}>
            <Paper variant="outlined" style={{paddingTop: '4px', paddingBottom: '4px', paddingLeft: '8px', backgroundColor: '#e8e8e8'}}>
                <Breadcrumbs separator="›">
                    <Link color="inherit" href={'/'}>
                        Estadísticas
                    </Link>
                    <Typography color="textPrimary">{slugToName(props.slug)}</Typography>
                </Breadcrumbs>
            </Paper>
        </Grid>
        <Grid item xs={12}>
            <Typography variant="h3" color="textPrimary">Estadísticas diarias {slugToName(props.slug)}</Typography>
        </Grid>
        <Grid item xs={12}>
            <CountryTable slug={props.slug} />
        </Grid>
    </Grid>

}


export default CountrySummary
