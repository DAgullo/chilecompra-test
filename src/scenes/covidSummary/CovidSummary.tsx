import React from "react";
import CountryCard from "./components/CountryCard";
import axios from 'axios';
import {Grid, Paper, Backdrop, CircularProgress} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";

class CovidSummary extends React.Component {

    state = {
        countries: [],
        loading: false
    }

    componentDidMount() {
        this.setState({loading: true});
        axios.get(`${process.env.BACKEND_ADDR}:${process.env.BACKEND_PORT}/statistics`)
            .then(res => {
                const countries =  res.data.Countries;
                this.setState({countries: countries, loading: false});
            })
    }

    render() {
        return <Grid container justify="flex-start" spacing={2}>
            <Grid item xs={12}>
                <Backdrop open={this.state.loading} style={{zIndex: 999}}>
                    <CircularProgress />
                </Backdrop>
            </Grid>
            <Grid item xs={12}>
                <Paper variant="outlined" style={{paddingTop: '4px', paddingBottom: '4px', paddingLeft: '8px', backgroundColor: '#e8e8e8'}}>
                    <Breadcrumbs separator="›">
                        <Typography color="textPrimary">Estadísticas</Typography>
                    </Breadcrumbs>
                </Paper>
            </Grid>
            <Grid item xs={12}>
                <Typography variant="h3" color="textPrimary">Estadísticas Generales COVID-19</Typography>
            </Grid>
            {this.state.countries.map((row: any) => (
                <Grid item xs={3}>
                    <CountryCard country={row.Country}
                         slug={row.Slug}
                         totalConfirmed={row.TotalConfirmed}
                         totalDeaths={row.TotalDeaths}
                         totalRecovered={row.TotalRecovered}
                    />
                </Grid>
            ))}
        </Grid>
    }

}

export default CovidSummary
