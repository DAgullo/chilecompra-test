import React from "react";
import {Card, CardHeader, CardContent, Typography, IconButton, Tooltip} from '@material-ui/core';
import {KeyboardArrowRight} from "@material-ui/icons";
import {Link} from "react-router-dom";

type Props = {
    country: string,
    slug: string,
    totalConfirmed: number,
    totalDeaths: number,
    totalRecovered: number
}

function CountryCard (props: Props) {
    return <Card>
        <CardHeader title={props.country} action={<Tooltip title={"Detalles"} arrow>
            <Link to={`/country/${props.slug}`}>
                <IconButton>
                    <KeyboardArrowRight />
                </IconButton>
            </Link>
        </Tooltip>}/>
        <CardContent>
            <Typography>
                {`Casos totales: ${props.totalConfirmed}`}
            </Typography>
            <Typography>
                {`Total de muertes: ${props.totalDeaths}`}
            </Typography>
            <Typography>
                {`total de recuperaciones: ${props.totalRecovered}`}
            </Typography>
        </CardContent>
    </Card>
}

export default CountryCard
