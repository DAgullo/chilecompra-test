import Grid from "@material-ui/core/Grid";
import React from "react";
import CovidSummary from "./covidSummary/CovidSummary";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import CountrySummary from "./countrySummary/CountrySummary";

function App() {
  return (
    <div>
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <Grid container spacing={1} style={{paddingLeft: '8px', paddingRight: '8px'}}>
          <Grid item xs={12}>
              <BrowserRouter>
                  <Switch>
                      <Route key="country" path="/country/:slug" render={( { match } ) => <CountrySummary slug={match.params.slug} /> } />
                      <Route exact path="/">
                          <CovidSummary />
                      </Route>
                  </Switch>
              </BrowserRouter>
          </Grid>
        </Grid>
    </div>
  );
}

export default App;
