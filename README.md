# Prueba Técnica Chilecompra - Frontend

## About

Proyecto simple de React.js para el Frontend de la prueba técnica para Chilecompra

## Instalación

Al extraer el proyecto, ejecutar **yarn** (es necesario tener instalado yarn, de no ser así ejecutar *npm install yarn*). Acto seguido se debe crear un archivo .env que contenga la siguiente información:

'''
BACKEND_ADDR=http://localhost #host y protocolo http donde se ubica el backend
BACKEND_PORT=8080 #puerto del backend
'''

## Ejecución

Un servlet de desarrollo se puede crear corriendo el comando **npm run start**, para correr una instancia de producción, es necesario configurar un servidor nginx con politicas de Catch-All.

## Endpoints

La página principal '/' muestra las tarjetas de cada país con sus estadísticas de COVID-19, al hacer clic en los botones de detalle dentro de estas se llega a la url '/country/:slug', que muestra la tabla con los detalles diarios.


